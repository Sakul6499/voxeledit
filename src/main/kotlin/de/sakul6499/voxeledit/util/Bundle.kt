package de.sakul6499.voxeledit.util

open class Bundle<F, S>(var first: F? = null, var second: S? = null)