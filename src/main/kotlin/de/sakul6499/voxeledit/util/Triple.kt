package de.sakul6499.voxeledit.util

open class Triple<F, S, T>(var first: F? = null, var second: S? = null, var third: T? = null)