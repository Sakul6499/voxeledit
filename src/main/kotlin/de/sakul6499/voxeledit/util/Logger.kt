package de.sakul6499.voxeledit.util

class Logger {
    enum class LogType(val prefix: String) {
        INFO("[I]"),
        DEBUG("[#]"),
        WARNING("[!]"),
        ERROR("[!!!]")
    }

    companion object {
        private fun log(type: LogType, message: String) {
            println(type.prefix + " " + message)
        }

        fun info(message: String) {
            log(LogType.INFO, message)
        }

        fun debug(message: String) {
            log(LogType.DEBUG, message)
        }

        fun warning(message: String) {
            log(LogType.WARNING, message)
        }

        fun error(message: String) {
            log(LogType.ERROR, message)
        }
    }
}
