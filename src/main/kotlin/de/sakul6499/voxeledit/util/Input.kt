package de.sakul6499.voxeledit.util

import org.bukkit.Bukkit
import org.bukkit.block.data.BlockData
import org.bukkit.command.CommandSender

class Input {
    companion object {
        fun fetchFromInt(l: String, sender: CommandSender? = null, checkBoundary: Boolean = true): Int? = try {
            val i = Integer.parseInt(l)

            if (checkBoundary && i < 0) {
                sender?.sendMessage("Number must be greater or equal zero!")
                null
            } else i
        } catch (e: NumberFormatException) {
            sender?.sendMessage("Invalid number!")
            null
        }

        fun fetchBlockDataFromString(l: String, sender: CommandSender? = null): Array<BlockData> {
            var output: Array<BlockData> = arrayOf()

            val literalMaterials = l.split(";")
            literalMaterials.forEach {
                try {
                    output += Bukkit.createBlockData(it)
                } catch (e: IllegalArgumentException) {
                    sender?.sendMessage("Invalid block data: $it")
                }
            }

            return output
        }
    }
}