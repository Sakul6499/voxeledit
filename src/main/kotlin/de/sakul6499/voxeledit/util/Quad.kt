package de.sakul6499.voxeledit.util

open class Quad<F, S, T, E>(var first: F? = null, var second: S? = null, var third: T? = null, var fourth: E? = null)