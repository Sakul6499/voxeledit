package de.sakul6499.voxeledit

import de.sakul6499.voxeledit.util.Input
import org.bukkit.Bukkit
import org.bukkit.Location
import org.bukkit.Material
import org.bukkit.block.data.BlockData
import org.bukkit.command.Command
import org.bukkit.command.CommandExecutor
import org.bukkit.command.CommandSender
import org.bukkit.command.TabCompleter
import org.bukkit.enchantments.Enchantment
import org.bukkit.entity.Player
import org.bukkit.event.EventHandler
import org.bukkit.event.Listener
import org.bukkit.event.block.Action
import org.bukkit.event.player.PlayerInteractEvent
import org.bukkit.inventory.ItemStack
import org.bukkit.util.Vector

class BuildersWand : CommandExecutor, TabCompleter, Listener {

    companion object {
        private const val itemName = "Builder's Wand"

        fun registerSelf() {
            val self = BuildersWand()
            Bukkit.getPluginManager().registerEvents(self, VoxelEdit.instance)
            val cmd = Bukkit.getPluginCommand("builderswand")!!
            cmd.setExecutor(self)
            cmd.tabCompleter = self
        }
    }

    private val map: MutableMap<Player, Int> = mutableMapOf()

    private fun checkCoords(
        x: Double,
        y: Double,
        z: Double,
        blockData: BlockData,
        location: Location,
        direction: Vector
    ) {
        var newLocation = location.clone().add(x, y, z)

        if (newLocation.block.blockData.asString == blockData.asString) {
            newLocation = newLocation.add(direction)
            if (newLocation.block.blockData.material == Material.AIR) {
                newLocation.block.blockData = blockData
            }
        }
    }

    @EventHandler
    fun onAction(event: PlayerInteractEvent) {
        if (event.action != Action.RIGHT_CLICK_BLOCK || event.item?.itemMeta
                ?.displayName != itemName || !map.containsKey(event.player)
        ) return

        // Cancel event (e.g. prevent block placement)
        event.isCancelled = true

        val blockData = event.clickedBlock!!.blockData
        val location = event.clickedBlock!!.location
        val direction = event.blockFace.direction
        val blocksLeft = map[event.player]!!

        val xCoord: Boolean = event.blockFace.direction.x != 0.0
        val yCoord: Boolean = event.blockFace.direction.y != 0.0
        val zCoord: Boolean = event.blockFace.direction.z != 0.0

        for (i in -blocksLeft..blocksLeft) {
            for (j in -blocksLeft..blocksLeft) {
                when {
                    xCoord -> {
                        checkCoords(0.0, i.toDouble(), j.toDouble(), blockData, location, direction)
                    }
                    yCoord -> {
                        checkCoords(i.toDouble(), 0.0, j.toDouble(), blockData, location, direction)
                    }
                    zCoord -> {
                        checkCoords(i.toDouble(), j.toDouble(), 0.0, blockData, location, direction)
                    }
                    else -> {
                        event.player.sendMessage("Invalid block face ($xCoord $yCoord $zCoord)")
                    }
                }
            }
        }
    }

    override fun onCommand(sender: CommandSender, command: Command, label: String, args: Array<out String>): Boolean {
        if (sender !is Player) return false

        if (args.isEmpty()) {
            val wand = ItemStack(Material.STICK, 1)
            val meta = Bukkit.getItemFactory().getItemMeta(Material.STICK)!!
            meta.setDisplayName(itemName)
            meta.addEnchant(Enchantment.VANISHING_CURSE, 100, true)
            wand.itemMeta = meta

            sender.inventory.addItem(wand)
            sender.sendMessage("You received a builder's wand!")

            if (!map.containsKey(sender)) {
                map[sender] = 5
            }
            val count = map[sender]
            sender.sendMessage("Current block count: $count")
        } else {
            val count = Input.fetchFromInt(args[0], sender, true)
            if (count == null || count < 0) {
                sender.sendMessage("Number must be positive!")
                return true
            }

            map[sender] = count
            sender.sendMessage("Set block count to ${count}!")
        }

        return true
    }

    override fun onTabComplete(
        sender: CommandSender,
        command: Command,
        alias: String,
        args: Array<out String>
    ): MutableList<String> {
        val list = arrayListOf<String>()
        if (args.size == 1) {
            for (i in 0..999) {
                val arg = args[0]
                val iS = i.toString()
                val iSs = if (i < 10) "00$i" else if (i < 99) "0$i" else i.toString()
                if (arg.isNotBlank()) {
                    if (args[0].startsWith(iS)) list.add(iSs)
                } else {
                    list.add(iSs)
                }
            }
        }
        return list
    }
}