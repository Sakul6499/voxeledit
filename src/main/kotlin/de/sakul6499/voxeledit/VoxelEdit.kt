package de.sakul6499.voxeledit

import com.mashape.unirest.http.HttpResponse
import com.mashape.unirest.http.Unirest
import de.sakul6499.voxeledit.command.SphereCommand
import de.sakul6499.voxeledit.command.UndoCommand
import de.sakul6499.voxeledit.commands.VoxelEditCommand
import de.sakul6499.voxeledit.util.Logger
import org.bukkit.Bukkit
import org.bukkit.event.EventHandler
import org.bukkit.event.Listener
import org.bukkit.event.entity.PlayerDeathEvent
import org.bukkit.event.player.PlayerJoinEvent
import org.bukkit.event.player.PlayerQuitEvent
import org.bukkit.plugin.java.JavaPlugin

class VoxelEdit : JavaPlugin(), Listener {
    companion object {
        lateinit var instance: VoxelEdit
            private set
    }

    override fun onLoad() {
        super.onLoad()
        Logger.info("Loading VoxelEdit ...")

        instance = this

        Logger.info("Loaded VoxelEdit!")
    }

    override fun onEnable() {
        super.onEnable()
        Logger.info("Starting VoxelEdit ...")

        // Register commands
        Bukkit.getPluginCommand("voxeledit")!!.setExecutor(VoxelEditCommand())
        SphereCommand.registerSelf()
        UndoCommand.registerSelf()
        BuildersWand.registerSelf()

        // register self event handlers
        Bukkit.getPluginManager().registerEvents(this, this)

        // register VE-Mode event handler
        Bukkit.getPluginManager().registerEvents(VEMode, this)

        // register VE-Tool event handler
        Bukkit.getPluginManager().registerEvents(VETool, this)

        Logger.info("Started VoxelEdit!")
    }

    @EventHandler
    fun onPlayerDeath(event: PlayerDeathEvent) {
        val response: HttpResponse<String> =
            Unirest.post("https://discordapp.com/api/webhooks/685953324394217515/MnThJsYk4fqcfF2sM_Ozd9_73aYEloN_ohRLlLf41QNW9qx226C8SEkzxY5Ne4s9o0L1?wait=true")
                .header(
                    "cookie",
                    "__cfduid=d37e3efa7dd9845111c0766848558e5881583611934; __cfruid=9d0c28886597f3cadcf8543ea2613a58852f20e6-1583611934"
                )
                .header("content-type", "multipart/form-data; boundary=---011000010111000001101001")
                .body("-----011000010111000001101001\r\nContent-Disposition: form-data; name=\"content\"\r\n\r\nUser ${event.entity.name} died!\r\n${event.deathMessage}\r\n-----011000010111000001101001\r\nContent-Disposition: form-data; name=\"username\"\r\n\r\nActivity\r\n-----011000010111000001101001\r\nContent-Disposition: form-data; name=\"tls\"\r\n\r\nfalse\r\n-----011000010111000001101001--\r\n")
                .asString()
        println("Response: $response")
    }

    @EventHandler
    fun onPlayerJoin(event: PlayerJoinEvent) {
        val response: HttpResponse<String> =
            Unirest.post("https://discordapp.com/api/webhooks/685953324394217515/MnThJsYk4fqcfF2sM_Ozd9_73aYEloN_ohRLlLf41QNW9qx226C8SEkzxY5Ne4s9o0L1?wait=true")
                .header(
                    "cookie",
                    "__cfduid=d37e3efa7dd9845111c0766848558e5881583611934; __cfruid=9d0c28886597f3cadcf8543ea2613a58852f20e6-1583611934"
                )
                .header("content-type", "multipart/form-data; boundary=---011000010111000001101001")
                .body("-----011000010111000001101001\r\nContent-Disposition: form-data; name=\"content\"\r\n\r\nUser ${event.player.name} joined the server!\r\n-----011000010111000001101001\r\nContent-Disposition: form-data; name=\"username\"\r\n\r\nActivity\r\n-----011000010111000001101001\r\nContent-Disposition: form-data; name=\"tls\"\r\n\r\nfalse\r\n-----011000010111000001101001--\r\n")
                .asString()
        println("Response: $response")
    }

    @EventHandler
    fun onPlayerQuit(event: PlayerQuitEvent) {
        val response: HttpResponse<String> =
            Unirest.post("https://discordapp.com/api/webhooks/685953324394217515/MnThJsYk4fqcfF2sM_Ozd9_73aYEloN_ohRLlLf41QNW9qx226C8SEkzxY5Ne4s9o0L1?wait=true")
                .header(
                    "cookie",
                    "__cfduid=d37e3efa7dd9845111c0766848558e5881583611934; __cfruid=9d0c28886597f3cadcf8543ea2613a58852f20e6-1583611934"
                )
                .header("content-type", "multipart/form-data; boundary=---011000010111000001101001")
                .body("-----011000010111000001101001\r\nContent-Disposition: form-data; name=\"content\"\r\n\r\nUser ${event.player.name} left the server!\r\n-----011000010111000001101001\r\nContent-Disposition: form-data; name=\"username\"\r\n\r\nActivity\r\n-----011000010111000001101001\r\nContent-Disposition: form-data; name=\"tls\"\r\n\r\nfalse\r\n-----011000010111000001101001--\r\n")
                .asString()
        println("Response: $response")
    }
}
