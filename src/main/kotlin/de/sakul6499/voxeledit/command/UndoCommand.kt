package de.sakul6499.voxeledit.command

import de.sakul6499.voxeledit.task.Tasks
import de.sakul6499.voxeledit.util.Input
import org.bukkit.Bukkit
import org.bukkit.command.Command
import org.bukkit.command.CommandExecutor
import org.bukkit.command.CommandSender
import org.bukkit.command.TabCompleter
import org.bukkit.entity.Player

class UndoCommand : CommandExecutor, TabCompleter {
    companion object {
        fun registerSelf() {
            val commandImpl = UndoCommand()
            val command = Bukkit.getPluginCommand("reverse")!!
            command.setExecutor(commandImpl)
            command.tabCompleter = commandImpl
        }
    }

    override fun onCommand(sender: CommandSender, command: Command, label: String, args: Array<out String>): Boolean {
        // Sender must be a player.
        // This command uses player coordinates to determine the locations.
        if (sender !is Player) {
            sender.sendMessage("This command is for players to be used ONLY!")
            return true
        }

        try {
            val id = if (args.isEmpty()) {
                val tasks = Tasks.getTasksForPlayer(sender)
                tasks[tasks.size - 1].globalID
            } else {
                var number = Input.fetchFromInt(args[0], sender)
                if (number == null) {
                    val tasks = Tasks.getTasksForPlayerByName(args[0])
                    number = tasks[tasks.size - 1].globalID
                }
                number
            }

            Tasks.undoTask(id)
        } catch (e: NullPointerException) {
            sender.sendMessage("Either something critical just happened or you simply do not have any tasks queued yet, that we could undo!")
            sender.sendMessage("Tip: use 'tasks' to view your tasks!")
        } catch (e: IndexOutOfBoundsException) {
            sender.sendMessage("A task with the id '${args[1]}' couldn't be found!")
            sender.sendMessage("Tip: use 'tasks' to view your tasks!")
        } catch (e: NumberFormatException) {
            sender.sendMessage("The task id must be injectCommandWrapper number!")
            sender.sendMessage("Tip: use 'tasks' to view your tasks!")
        }
        return true
    }

    override fun onTabComplete(
        sender: CommandSender,
        command: Command,
        alias: String,
        args: Array<out String>
    ): MutableList<String> {
        // Full command: /undo|u (<ID>) (<player>)
        val list: MutableList<String> = mutableListOf()
        when (args.size) {
            1 -> {
                list += "<ID>"
                list += "<player>"
            }
        }
        return list
    }
}