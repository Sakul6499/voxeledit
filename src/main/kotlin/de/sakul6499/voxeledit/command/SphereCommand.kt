package de.sakul6499.voxeledit.command

import de.sakul6499.voxeledit.java.MaterialsToArray
import de.sakul6499.voxeledit.task.TaskManager
import de.sakul6499.voxeledit.task.Tasks
import de.sakul6499.voxeledit.task.impl.SphereTask
import de.sakul6499.voxeledit.util.Input
import org.bukkit.Bukkit
import org.bukkit.command.Command
import org.bukkit.command.CommandExecutor
import org.bukkit.command.CommandSender
import org.bukkit.command.TabCompleter
import org.bukkit.entity.Player
import org.bukkit.util.Vector

class SphereCommand : CommandExecutor, TabCompleter {
    companion object {
        fun registerSelf() {
            val commandImpl = SphereCommand()
            val command = Bukkit.getPluginCommand("sphere")!!
            command.setExecutor(commandImpl)
            command.tabCompleter = commandImpl
        }
    }

    override fun onCommand(sender: CommandSender, command: Command, label: String, args: Array<out String>): Boolean {
        // Sender must be a player.
        // This command uses player coordinates to determine the locations.
        if (sender !is Player) {
            sender.sendMessage("This command is for players to be used ONLY!")
            return true
        }
        if (args.size <= 1) return false

        val loc = sender.location
        val world = loc.world!!

        val locX = loc.blockX
        val locY = loc.blockY
        val locZ = loc.blockZ

        val midPoint = Vector(locX, locY, locZ)
        val radius = Input.fetchFromInt(args[0], sender)
        val blockData = Input.fetchBlockDataFromString(args[1], sender)

        var hollow = false
        if (args.size >= 3) {
            if (args[2].toLowerCase() == "hollow") {
                hollow = true
            } else {
                sender.sendMessage("Didn't get '${args[2]}'! [Mean 'hollow'?]")
            }
        }

        if (radius != null && blockData.isNotEmpty()) {
            // Create task
            val task = SphereTask(world, midPoint, radius, blockData, hollow)
            // Register Task
            val taskInfo = TaskManager.instance.registerTask(task, sender.uniqueId)
            // Inform player about task
            sender.sendMessage("Task registered as:")
            sender.sendMessage("Global ID: ${taskInfo.globalID}")
            sender.sendMessage("Player ID: ${taskInfo.playerID}")
            sender.sendMessage("Blocks to process: ${task.overallBlocksToProcess}")
            // Teleport player on-top of structure
            sender.teleport(loc.add(0.0, radius.toDouble() + 1, 0.0))
        }

        return true
    }

    override fun onTabComplete(
        sender: CommandSender,
        command: Command,
        alias: String,
        args: Array<out String>
    ): MutableList<String> {
        // Full command: /sphere|s <radius> <blockName(:blockDataIndex)>|<blockIndex(:blockDataIndex)>(; ...) (hollow)
        val list: MutableList<String> = mutableListOf()
        when (args.size) {
            1 -> {
                list += "<size>"
            }
            2 -> {
                for (material in MaterialsToArray.GetAllMaterials()) {
                    if (!material.isBlock) continue
                    val materialName = material.toString().toLowerCase()
                    if (!materialName.startsWith(args[1].toLowerCase())) continue   // TODO: should be contains
                    list += materialName
                }
            }
            3 -> {
                list += "hollow"
            }
        }
        return list
    }
}