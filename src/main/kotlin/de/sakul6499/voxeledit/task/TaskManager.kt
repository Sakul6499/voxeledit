package de.sakul6499.voxeledit.task

import de.sakul6499.voxeledit.VoxelEdit
import org.bukkit.Bukkit
import org.bukkit.scheduler.BukkitTask
import java.util.*

class TaskManager private constructor() {

    companion object {
        val instance = TaskManager()
        var task: BukkitTask? = null
        var blockPerTask = 4096
    }

    private val tasks = mutableListOf<TaskInfo>()
    private val finishedTasks = mutableListOf<TaskInfo>()
    private val playerCounter = mutableMapOf<UUID, Int>()
    private var globalCounter = 0

    fun registerTask(task: Task, requester: UUID): TaskInfo {
        // Get IDs
        val globalID = globalCounter++
        val playerID = playerCounter.getOrPut(requester, { 0 })
        // Create TaskInfo object
        val taskInfo = TaskInfo.fromDefault(
            task,
            globalID,
            playerID,
            requester
        )
        // Register/Add TaskInfo
        tasks.add(taskInfo)
        // Check status of task runner and start if necessary
        checkTaskRunner()
        // Return instance of created task info for further information
        return taskInfo
    }

    fun cancelTaskByGlobalID(globalID: Int): Boolean =
        tasks.removeIf { it.globalID == globalID }

    fun cancelTaskByPlayerID(playerID: Int): Boolean =
        tasks.removeIf { it.playerID == playerID }

    private fun checkTaskRunner() {
        if (task != null || tasks.isEmpty()) return
        task = Bukkit.getScheduler().runTask(VoxelEdit.instance, Runnable {
            val taskInfo = tasks.removeAt(0)
            val task = taskInfo.task

            if (taskInfo.undo) {
                task.undo()
            } else {
                task.process()
            }

            finishedTasks.add(taskInfo)

            if (tasks.isNotEmpty()) {
                checkTaskRunner()   // Note: recursive call
            }
        })
    }
}