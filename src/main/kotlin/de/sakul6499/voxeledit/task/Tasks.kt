package de.sakul6499.voxeledit.task

import de.sakul6499.voxeledit.clipboard.Selection
import de.sakul6499.voxeledit.clipboard.SelectionData
import org.bukkit.entity.Player

/**
 * Tasks.
 * Used to schedule and manage different tasks.
 */
object Tasks {

    var BlocksPerSecond: Int = 4096
    var SelectionBlocksPerSecond: Int = 8192

    private var idCounter = 0
    private val tasks: MutableList<TaskInfo> = mutableListOf()
    private val selections: MutableList<SelectionData> = mutableListOf()

    init {
//        Bukkit.getServer().scheduler.runTaskTimer(VoxelEdit.instance, {
//            val taskCount = tasks.count { !it.finished && !it.canceled }
//            if (taskCount > 0) {
//                // Blocks Per Task
//                val bpt = BlocksPerSecond / taskCount
//
//                println("Tasks:")
//                tasks.forEach{println(it)}
//
//                tasks.filterNot { it.finished || it.canceled }.forEach {
//                    val timeLeft = it.task.count(it.undo) / bpt
//
//                    for (i in 0..bpt) {
//                        if (it.undo) {
//                            if (!it.task.undo()) {
//                                it.notifier.sendMessage("#${it.globalID} undo task finished!")
//                                it.finished = true
//                                return@forEach
//                            }
//                            it.loops++
//                        } else {
//                            if (!it.task.process()) {
//                                it.notifier.sendMessage("#${it.globalID} task finished!")
//                                it.finished = true
//                                return@forEach
//                            }
//                            it.loops++
//                        }
//                    }
//
//                    it.notifier.sendMessage(
//                        "#${it.globalID} BlocksPerSecond: ${BlocksPerSecond} BPT: $bpt [${timeLeft}s - ${it.task
//                            .count(it.undo)}b]"
//                    )
//                }
//            } else {
//                Thread.sleep(1000)
//            }
//        } as () -> Unit, 0, 20)
//
//        Bukkit.getServer().scheduler.runTaskTimer(VoxelEdit.instance, {
//            val selection = selections.filter { it.shouldSelect && (!it.finished && !it.canceled) }
//            if (selection.isNotEmpty()) {
//                val sbpt = SelectionBlocksPerSecond / selection.size
//
//                selection.forEach {
//                    val timeLeft = (it.selection.blocks - it.loops) / sbpt
//
//                    for (i in 0..sbpt) {
//                        if (!it.selection.select()) {
//                            it.notifier.sendMessage("#${it.id} selection task finished!")
//                            it.finished = true
//                            return@forEach
//                        }
//
//                        it.loops++
//                    }
//
//                    it.notifier.sendMessage(
//                        "#${it.id} SBPS: ${Tasks.BlocksPerSecond} SBPT: $sbpt [${timeLeft}s - ${it.selection.count()}b]"
//                    )
//                }
//            } else {
//                Thread.sleep(1000)
//            }
//        } as () -> Unit, 0, 20)
    }

    fun addTask(task: Task, notifier: Player): Int {
        return 0
//        val taskData = TaskInfo(task, ++idCounter, 0, notifier) // TODO: Player Counter instead of zero!
//
//        notifier.sendMessage("Added task #${taskData.globalID}!")
//        notifier.sendMessage("Block to process: ${taskData.task.overallBlocksToProcess}")
//        tasks.add(taskData)
//
//        return taskData.globalID
    }

    fun cancelTask(id: Int) {
//        val task = tasks.firstOrNull { it.globalID == id } ?: throw IllegalStateException("Task with id $id not found!")
//        task.notifier.sendMessage("Cancelling task: $id")
//
//        task.canceled = true
    }

    fun undoTask(id: Int) {
//        val task = tasks.firstOrNull { it.globalID == id } ?: throw IllegalStateException("Task with id $id not found!")
//        task.notifier.sendMessage("Undo task: $id")
//
//        task.undo = !task.undo
//        if (task.finished) task.finished = false
//        if (task.canceled) task.canceled = false
    }

    fun addSelection(selection: Selection, notifier: Player): Int {
        val selectionData = SelectionData(selection, ++idCounter, notifier)

        notifier.sendMessage("Added Selection task #${selectionData.id}!")
        notifier.sendMessage("Blocks to process: ${selectionData.selection.blocks}")
        selections.add(selectionData)

        return selectionData.id
    }

    fun cancelSelection(id: Int) {
        val selection = selections.firstOrNull { it.id == id }
            ?: throw IllegalStateException("Selection with id $id not found!")
        selection.notifier.sendMessage("Cancelling selection: $id")

        selection.canceled = true
    }

    //    fun getTasksForPlayer(player: Player): List<TaskInfo> = tasks.filter { it.notifier == player }
    fun getTasksForPlayer(player: Player): List<TaskInfo> = tasks.filter { true }

    //    fun getTasksForPlayerByName(name: String): List<TaskInfo> = tasks.filter { it.notifier.name == name }
    fun getTasksForPlayerByName(name: String): List<TaskInfo> = tasks.filter { true }
    fun getTaskByID(id: Int): TaskInfo? = tasks.filter { it.globalID == id }.firstOrNull()

    fun getSelectionsForPlayer(player: Player): List<SelectionData> = selections.filter { it.notifier == player }
    fun getSelectionsForPlayerByName(name: String): List<SelectionData> = selections.filter { it.notifier.name == name }
    fun getTasksByID(id: Int): SelectionData? = selections.filter { it.id == id }.firstOrNull()
}