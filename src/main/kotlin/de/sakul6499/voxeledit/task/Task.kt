package de.sakul6499.voxeledit.task

import org.bukkit.World

/**
 * Task interface.
 * Used for defining specialized tasks.
 */
interface Task {
    /**
     * World.
     * Used to get blocks from a given location.
     */
    val world: World

    /**
     * How many blocks are needed to be processed.
     * This is the total/overall count.
     * Thus, blocks that are the same and do not need to be processed are INCLUDED.
     */
    val overallBlocksToProcess: Int

    /**
     * Queue for storing task actions.
     * While processing, elements will be removed from this collection and
     * changed accordingly.
     * While undoing, the previous state will be stored in here.
     */
    val processingQueue: MutableCollection<TaskAction>

    /**
     * Queue for undoing task actions.
     * While processing, blocks will be stored here in their original state,
     * before changing.
     * While undoing, blocks will be undone by removing elements from this collection.
     */
    val undoQueue: MutableCollection<TaskAction>

    /**
     * Counts how many blocks still need to be processed or undone.
     * @param undo if true, will return the count of blocks to be undone,
     *              otherwise the count of blocks to be processed
     * @return the currently left over blocks to process.
     */
    fun count(undo: Boolean = false): Int = if (undo) toBeUndone() else toBeProcessed()

    /**
     * Counts how many blocks need to be undone.
     * @return how many blocks need to be undone.
     */
    fun toBeUndone(): Int = undoQueue.size

    /**
     * Counts how many blocks need to be processed.
     * @return how many blocks need to be processed.
     */
    fun toBeProcessed(): Int = processingQueue.size

    /**
     * Starts processing all blocks/tasks.
     */
    fun process(): Boolean = DefaultTaskActions.process(processingQueue, undoQueue, world)

    /**
     * Undoes previously processed blocks/tasks.
     * A task first has to be {@code Task::process}ed, before undo has an effect.
     */
    fun undo(): Boolean = DefaultTaskActions.process(undoQueue, processingQueue, world)
}