package de.sakul6499.voxeledit.task

import de.sakul6499.voxeledit.VoxelEdit
import org.bukkit.Bukkit
import org.bukkit.World

/**
 * Defines a default task action to be used by most, if not all, tasks.
 */
object DefaultTaskActions {
    /**
     * Processing task.
     * Goes through all 'processingTasks' and places them back into 'undoTasks'.
     * @param processingTasks input tasks, all will be processed.
     * @param undoTasks output tasks, before processing, current block states will be stored as tasks in here.
     * @param world used to get a block from a location.
     * @return true, if everything did go well, false otherwise (most likely empty 'processingTasks').
     */
    fun process(
        processingTasks: MutableCollection<TaskAction>,
        undoTasks: MutableCollection<TaskAction>,
        world: World
    ): Boolean {
        if (processingTasks.isEmpty()) return false

        var blockCount = 0
        processingTasks.removeIf {
            if (blockCount > TaskManager.blockPerTask) return@removeIf false

            // Get actual block in world
            val block = it.location.toLocation(world).block
            // Add current state to undo array
            undoTasks.plus(TaskAction(it.location, block.blockData))
            // Set new block data
            block.blockData = it.changeTo
            // Increment counter
            blockCount++

            true
        }

        return true
    }
}