package de.sakul6499.voxeledit.task

import java.util.*

/**
 * Data class Task Info.
 * Stores additional information about tasks.
 * @param task Task the information is about.
 * @param globalID global id of the task.
 * @param playerID player id of the task.
 * @param requester player who requested this task.
 * @param undo whether the task is in undo mode or not.
 */
data class TaskInfo(
    val task: Task,
    val globalID: Int,
    val playerID: Int,
    var requester: UUID,
    var undo: Boolean = false
) {
    companion object {
        fun fromDefault(
            task: Task,
            globalID: Int,
            playerID: Int,
            requester: UUID
        ) = TaskInfo(
            task,
            globalID,
            playerID,
            requester,
            undo = false
        )
    }

    override fun toString(): String =
        "#$globalID-$playerID notifier: $requester ovbtp: ${task.overallBlocksToProcess} ${if (undo) "[UNDO]" else ""}"
}