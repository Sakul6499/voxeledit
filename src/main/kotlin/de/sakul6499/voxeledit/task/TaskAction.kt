package de.sakul6499.voxeledit.task

import org.bukkit.block.data.BlockData
import org.bukkit.util.Vector

/**
 * Data class Task Action.
 * Stores important data for all tasks to be used.
 * A location ({@code Vector}) indicates the location of a block
 * and an additional {@code BlockData} field describes the block data it should have after processing.
 */
data class TaskAction(
    /**
     * Location of the block.
     */
    val location: Vector,
    /**
     * Block data to be changed into.
     */
    val changeTo: BlockData
)