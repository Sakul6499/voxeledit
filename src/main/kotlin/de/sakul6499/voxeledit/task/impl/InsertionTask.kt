package de.sakul6499.voxeledit.task.impl

import de.sakul6499.voxeledit.task.Task
import de.sakul6499.voxeledit.task.TaskAction
import org.bukkit.World

/**
 * Insertion Task.
 * Simply inserts blocks where given in 'processingQueue'.
 * @param world World, used to get blocks from locations.
 * @param processingQueue blocks/tasks to process.
 */
class InsertionTask(override val world: World, override val processingQueue: MutableCollection<TaskAction>) :
    Task {
    override val overallBlocksToProcess: Int = count()
    override val undoQueue: MutableCollection<TaskAction> = mutableSetOf()
}