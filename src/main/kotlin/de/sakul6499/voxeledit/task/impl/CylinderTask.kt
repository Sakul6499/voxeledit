package de.sakul6499.voxeledit.task.impl

import de.sakul6499.voxeledit.task.Task
import de.sakul6499.voxeledit.task.TaskAction
import org.bukkit.World
import org.bukkit.block.data.BlockData
import org.bukkit.util.Vector
import java.util.*

/**
 * Cylinder Task.
 * Places blocks in a cylinder shape.
 * @param midPoint / center of the cylinder.
 * @param radius of the cylinder.
 * @param up upper coordinate of the cylinder.
 * @param down lower coordinate of the cylinder.
 * @param blockDataArray block data's to be placed in the cylinder.
 * @param hollow whether the cylinder is hollow on the inside or not.
 */
class CylinderTask(
    override val world: World,
    midPoint: Vector,
    radius: Int,
    up: Int,
    down: Int,
    blockDataArray: Array<BlockData>,
    hollow: Boolean = false
) : Task {
    override val overallBlocksToProcess: Int
    override val processingQueue: MutableCollection<TaskAction> = mutableSetOf()
    override val undoQueue: MutableCollection<TaskAction> = mutableSetOf()

    // Height
    private var yBegin: Int = midPoint.blockY - down
    private var yEnd: Int = midPoint.blockY + up

    private val xBegin: Int = midPoint.blockX - radius
    private val xEnd: Int = midPoint.blockX + radius

    private val zBegin: Int = midPoint.blockZ - radius
    private val zEnd: Int = midPoint.blockZ + radius

    init {
        // Normalize
        if (yBegin > 255) yBegin = 255
        if (yEnd < 0) yEnd = 0

        val random = Random()
        for (y in yEnd downTo yBegin) {
            for (x in xBegin..xEnd) {
                for (z in zBegin..zEnd) {
                    val currentPosition = Vector(x, y, z)
                    val distance = Vector(midPoint.blockX, y, midPoint.blockZ).distance(currentPosition).toInt()

                    if ((hollow && distance == radius) || (!hollow && distance <= radius)) {
                        val blockData = blockDataArray[random.nextInt(blockDataArray.size)]
                        val taskAction =
                            TaskAction(currentPosition, blockData)
                        processingQueue.add(taskAction)
                    }
                }
            }
        }

        overallBlocksToProcess = count()
    }
}