package de.sakul6499.voxeledit.task.impl

import de.sakul6499.voxeledit.task.Task
import de.sakul6499.voxeledit.task.TaskAction
import org.bukkit.World
import org.bukkit.block.data.BlockData
import org.bukkit.util.Vector
import java.util.*

/**
 * Voxel Task.
 * Places blocks in a voxel shape.
 * @param world World, used to get blocks from locations.
 * @param midPoint / center of the voxel.
 * @param radius of the voxel
 * @param blockDataArray possible block data's (materials) to be used in the voxel.
 * @param hollow whether the voxel should be hollow on the inside or not.
 */
class VoxelTask(
    override val world: World,
    midPoint: Vector,
    radius: Int,
    blockDataArray: Array<BlockData>,
    hollow: Boolean = false
) : Task {
    override val overallBlocksToProcess: Int
    override val processingQueue: MutableCollection<TaskAction> = mutableSetOf()
    override val undoQueue: MutableCollection<TaskAction> = mutableSetOf()

    // Height
    private var yBegin: Int = midPoint.blockY - radius
    private var yEnd: Int = midPoint.blockY + radius

    private val xBegin: Int = midPoint.blockX - radius
    private val xEnd: Int = midPoint.blockX + radius

    private val zBegin: Int = midPoint.blockZ - radius
    private val zEnd: Int = midPoint.blockZ + radius

    init {
        // Normalize
        if (yBegin > 255) yBegin = 255
        if (yEnd < 0) yEnd = 0

        val random = Random()
        var blockData: BlockData
        var location: Vector
        for (y in yEnd downTo yBegin) {
            if (hollow) {
                for (x in xBegin..xEnd) {
                    blockData = blockDataArray[random.nextInt(blockDataArray.size)]
                    location = Vector(x, y, zBegin)
                    processingQueue.add(TaskAction(location, blockData))

                    blockData = blockDataArray[random.nextInt(blockDataArray.size)]
                    location = Vector(x, y, zEnd)
                    processingQueue.add(TaskAction(location, blockData))
                }

                for (z in zBegin..zEnd) {
                    blockData = blockDataArray[random.nextInt(blockDataArray.size)]
                    location = Vector(xBegin, y, z)
                    processingQueue.add(TaskAction(location, blockData))

                    blockData = blockDataArray[random.nextInt(blockDataArray.size)]
                    location = Vector(xEnd, y, z)
                    processingQueue.add(TaskAction(location, blockData))
                }

                if (y == yBegin || y == yEnd) {
                    for (x in xBegin..xEnd) {
                        for (z in zBegin..zEnd) {
                            blockData = blockDataArray[random.nextInt(blockDataArray.size)]
                            location = Vector(x, y, z)
                            processingQueue.add(
                                TaskAction(location, blockData)
                            )
                        }
                    }
                }
            } else {
                for (x in xBegin..xEnd) {
                    for (z in zBegin..zEnd) {
                        location = Vector(x, y, z)
                        blockData = blockDataArray[random.nextInt(blockDataArray.size)]
                        processingQueue.add(
                            TaskAction(location, blockData)
                        )
                    }
                }
            }
        }

        overallBlocksToProcess = count()
    }
}