package de.sakul6499.voxeledit.task.impl

import de.sakul6499.voxeledit.task.Task
import de.sakul6499.voxeledit.task.TaskAction
import org.bukkit.World
import org.bukkit.block.data.BlockData
import org.bukkit.util.Vector
import java.util.*

/**
 * Sphere Task.
 * Places blocks in a spherical order.
 * @param world World, used to get blocks from locations.
 * @param midPoint / center of the sphere.
 * @param radius of the sphere.
 * @param blockDataArray block data's to be placed in the sphere.
 * @param hollow weather the sphere should be hollow (unchanged on the inside) or not.
 */
class SphereTask(
    override val world: World,
    midPoint: Vector,
    radius: Int,
    blockDataArray: Array<BlockData>,
    hollow: Boolean = false
) : Task {
    override val overallBlocksToProcess: Int
    override val processingQueue: MutableCollection<TaskAction> = mutableSetOf()
    override val undoQueue: MutableCollection<TaskAction> = mutableSetOf()

    // Height
    private var yBegin: Int = midPoint.blockY - radius
    private var yEnd: Int = midPoint.blockY + radius

    private val xBegin: Int = midPoint.blockX - radius
    private val xEnd: Int = midPoint.blockX + radius

    private val zBegin: Int = midPoint.blockZ - radius
    private val zEnd: Int = midPoint.blockZ + radius

    init {
        // Normalize
        if (yBegin > 255) yBegin = 255
        if (yEnd < 0) yEnd = 0

        val random = Random()
        for (y in yEnd downTo yBegin) {
            for (x in xBegin..xEnd) {
                for (z in zBegin..zEnd) {
                    val currentPosition = Vector(x, y, z)
                    val distance = midPoint.distance(currentPosition).toInt()

                    if ((hollow && distance == radius) || (!hollow && distance <= radius)) {
                        val blockData = blockDataArray[random.nextInt(blockDataArray.size)]
                        val taskAction =
                            TaskAction(currentPosition, blockData)
                        processingQueue.add(taskAction)
                    }
                }
            }
        }

        overallBlocksToProcess = count()
    }
}