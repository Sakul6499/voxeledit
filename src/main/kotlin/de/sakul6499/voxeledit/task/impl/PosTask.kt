package de.sakul6499.voxeledit.task.impl

import de.sakul6499.voxeledit.task.Task
import de.sakul6499.voxeledit.task.TaskAction
import org.bukkit.World
import org.bukkit.block.data.BlockData
import org.bukkit.util.Vector
import java.util.*

/**
 * Position Task.
 * Placed blocks between two coordinates.
 * Which positions coordinates are greater and lower will be calculated automatically.
 * @param world World, used to get blocks from a location.
 * @param pos1 first position.
 * @param pos2 second position.
 * @param blockDataArray block data array, containing all possible block data to be chosen from.
 * @param hollow whether the inside should be hollow or not.
 */
class PosTask(
    override val world: World,
    pos1: Vector,
    pos2: Vector,
    blockDataArray: Array<BlockData>,
    hollow: Boolean = false
) : Task {
    override val overallBlocksToProcess: Int
    override val processingQueue: MutableCollection<TaskAction> = mutableSetOf()
    override val undoQueue: MutableCollection<TaskAction> = mutableSetOf()

    // Height
    private var yBegin: Int
    private var yEnd: Int

    private val xBegin: Int
    private val xEnd: Int

    private val zBegin: Int
    private val zEnd: Int

    init {
        if (pos1.blockY > pos2.blockY) {
            yBegin = pos2.blockY
            yEnd = pos1.blockY
        } else {
            yBegin = pos1.blockY
            yEnd = pos2.blockY
        }
        if (yEnd > 255) yEnd = 255
        if (yBegin < 0) yBegin = 0

        if (pos1.blockX > pos2.blockX) {
            xBegin = pos2.blockX
            xEnd = pos1.blockX
        } else {
            xBegin = pos1.blockX
            xEnd = pos2.blockX
        }

        if (pos1.blockZ > pos2.blockZ) {
            zBegin = pos2.blockZ
            zEnd = pos1.blockZ
        } else {
            zBegin = pos1.blockZ
            zEnd = pos2.blockZ
        }

        val random = Random()
        var location: Vector
        for (y in yEnd downTo yBegin) {
            if (hollow) {
                for (x in xBegin..xEnd) {
                    var blockData = blockDataArray[random.nextInt(blockDataArray.size)]
                    location = Vector(x, y, zBegin)
                    processingQueue.add(TaskAction(location, blockData))

                    blockData = blockDataArray[random.nextInt(blockDataArray.size)]
                    location = Vector(x, y, zEnd)
                    processingQueue.add(TaskAction(location, blockData))
                }

                for (z in zBegin..zEnd) {
                    var blockData = blockDataArray[random.nextInt(blockDataArray.size)]
                    location = Vector(xBegin, y, z)
                    processingQueue.add(TaskAction(location, blockData))

                    blockData = blockDataArray[random.nextInt(blockDataArray.size)]
                    location = Vector(xEnd, y, z)
                    processingQueue.add(TaskAction(location, blockData))
                }

                if (y == yBegin || y == yEnd) {
                    for (x in xBegin..xEnd) {
                        for (z in zBegin..zEnd) {
                            val blockData = blockDataArray[random.nextInt(blockDataArray.size)]
                            location = Vector(x, y, z)
                            processingQueue.add(
                                TaskAction(location, blockData)
                            )
                        }
                    }
                }
            } else {
                for (x in xBegin..xEnd) {
                    for (z in zBegin..zEnd) {
                        val blockData = blockDataArray[random.nextInt(blockDataArray.size)]
                        location = Vector(x, y, z)

                        processingQueue.add(
                            TaskAction(location, blockData)
                        )
                    }
                }
            }
        }

        overallBlocksToProcess = count()
    }
}