package de.sakul6499.voxeledit.task.impl

import de.sakul6499.voxeledit.task.Task
import de.sakul6499.voxeledit.task.TaskAction
import org.bukkit.World
import org.bukkit.block.data.BlockData
import org.bukkit.util.Vector
import java.util.*

/**
 * Replace Position Task.
 * Takes two positions, a filter and a replace array.
 * Ever block matching filter array will be replaced by one in the replace array,
 * within the two positions.
 * Which position is greater and lower is calculated automatically.
 * @param world World, used to get blocks from locations.
 * @param pos1 first position.
 * @param pos2 second position.
 * @param filter used to match blocks.
 * @param replace used to replace blocks.
 * @param hollow whether the inside should be hollow or not.
 */
class ReplacePosTask(
    override val world: World,
    pos1: Vector,
    pos2: Vector,
    filter: Array<BlockData>,
    replace: Array<BlockData>,
    hollow: Boolean = false
) : Task {
    override val overallBlocksToProcess: Int
    override val processingQueue: MutableCollection<TaskAction> = mutableSetOf()
    override val undoQueue: MutableCollection<TaskAction> = mutableSetOf()

    // Height
    private var yBegin: Int
    private var yEnd: Int

    private val xBegin: Int
    private val xEnd: Int

    private val zBegin: Int
    private val zEnd: Int

    init {
        if (pos1.blockY > pos2.blockY) {
            yBegin = pos2.blockY
            yEnd = pos1.blockY
        } else {
            yBegin = pos1.blockY
            yEnd = pos2.blockY
        }
        if (yBegin > 255) yBegin = 255
        if (yEnd < 0) yEnd = 0

        if (pos1.blockX > pos2.blockX) {
            xBegin = pos2.blockX
            xEnd = pos1.blockX
        } else {
            xBegin = pos1.blockX
            xEnd = pos2.blockX
        }

        if (pos1.blockZ > pos2.blockZ) {
            zBegin = pos2.blockZ
            zEnd = pos1.blockZ
        } else {
            zBegin = pos1.blockZ
            zEnd = pos2.blockZ
        }

        val random = Random()
        var location: Vector
        for (y in yEnd downTo yBegin) {
            if (hollow) {
                for (x in xBegin..xEnd) {
                    location = Vector(x, y, zBegin)
                    var block = location.toLocation(world).block
                    if (filter.any { it.asString == block.blockData.asString }) {
                        val blockData = replace[random.nextInt(replace.size)]
                        processingQueue.add(
                            TaskAction(location, blockData)
                        )
                    }

                    location = Vector(x, y, zEnd)
                    block = location.toLocation(world).block
                    if (filter.any { it.asString == block.blockData.asString }) {
                        val blockData = replace[random.nextInt(replace.size)]
                        processingQueue.add(
                            TaskAction(location, blockData)
                        )
                    }
                }

                for (z in zBegin..zEnd) {
                    location = Vector(xBegin, y, z)
                    var block = location.toLocation(world).block
                    if (filter.any { it.asString == block.blockData.asString }) {
                        val blockData = replace[random.nextInt(replace.size)]
                        processingQueue.add(
                            TaskAction(location, blockData)
                        )
                    }

                    location = Vector(xEnd, y, z)
                    block = location.toLocation(world).block
                    if (filter.any { it.asString == block.blockData.asString }) {
                        val blockData = replace[random.nextInt(replace.size)]
                        processingQueue.add(
                            TaskAction(location, blockData)
                        )
                    }
                }

                if (y == yBegin || y == yEnd) {
                    for (x in xBegin..xEnd) {
                        for (z in zBegin..zEnd) {
                            location = Vector(x, y, z)
                            val block = location.toLocation(world).block
                            if (filter.any { it.asString == block.blockData.asString }) {
                                val blockData = replace[random.nextInt(replace.size)]
                                processingQueue.add(
                                    TaskAction(location, blockData)
                                )
                            }
                        }
                    }
                }
            } else {
                for (x in xBegin..xEnd) {
                    for (z in zBegin..zEnd) {
                        location = Vector(x, y, z)
                        val block = location.toLocation(world).block

                        if (filter.any { it.asString == block.blockData.asString }) {
                            val blockData = replace[random.nextInt(replace.size)]
                            processingQueue.add(
                                TaskAction(location, blockData)
                            )
                        }
                    }
                }
            }
        }

        overallBlocksToProcess = count()
    }
}