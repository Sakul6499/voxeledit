package de.sakul6499.voxeledit.task.impl

import de.sakul6499.voxeledit.task.Task
import de.sakul6499.voxeledit.task.TaskAction
import org.bukkit.World
import org.bukkit.block.data.BlockData
import org.bukkit.util.Vector
import java.util.*

/**
 * Tower Task.
 * Places blocks in a tower shape.
 * @param world World, used to get blocks from locations.
 * @param midPoint / center of the tower.
 * @param radius of the tower.
 * @param up upper limit of the tower.
 * @param down lower limit of the tower.
 * @param blockDataArray possible block data's (materials) to be used in the tower.
 * @param hollow whether the tower should be hollow on the inside or not.
 */
class TowerTask(
    override val world: World,
    midPoint: Vector,
    radius: Int,
    up: Int,
    down: Int,
    blockDataArray: Array<BlockData>,
    hollow: Boolean = false
) : Task {
    override val overallBlocksToProcess: Int
    override val processingQueue: MutableCollection<TaskAction> = mutableSetOf()
    override val undoQueue: MutableCollection<TaskAction> = mutableSetOf()

    // Height
    private var yBegin: Int = midPoint.blockY - down
    private var yEnd: Int = midPoint.blockY + up

    private val xBegin: Int = midPoint.blockX - radius
    private val xEnd: Int = midPoint.blockX + radius

    private val zBegin: Int = midPoint.blockZ - radius
    private val zEnd: Int = midPoint.blockZ + radius

    init {
        // Normalize heights (y-coordinate)
        if (yBegin > 255) yBegin = 255
        if (yEnd < 0) yEnd = 0

        val random = Random()
        var blockData: BlockData
        var location: Vector
        for (y in yEnd downTo yBegin) {
            if (hollow) {
                for (x in xBegin..xEnd) {
                    blockData = blockDataArray[random.nextInt(blockDataArray.size)]
                    location = Vector(x, y, zBegin)
                    processingQueue.add(TaskAction(location, blockData))

                    blockData = blockDataArray[random.nextInt(blockDataArray.size)]
                    location = Vector(x, y, zEnd)
                    processingQueue.add(TaskAction(location, blockData))
                }

                for (z in zBegin..zEnd) {
                    blockData = blockDataArray[random.nextInt(blockDataArray.size)]
                    location = Vector(xBegin, y, z)
                    processingQueue.add(TaskAction(location, blockData))

                    blockData = blockDataArray[random.nextInt(blockDataArray.size)]
                    location = Vector(xEnd, y, z)
                    processingQueue.add(TaskAction(location, blockData))
                }
            } else {
                for (x in xBegin..xEnd) {
                    for (z in zBegin..zEnd) {
                        blockData = blockDataArray[random.nextInt(blockDataArray.size)]
                        location = Vector(x, y, z)
                        processingQueue.add(
                            TaskAction(location, blockData)
                        )
                    }
                }
            }
        }

        overallBlocksToProcess = count()
    }
}